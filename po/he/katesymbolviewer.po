# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2014.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: katesymbolviewer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-11 01:45+0000\n"
"PO-Revision-Date: 2017-05-16 06:36-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Zanata 3.9.6\n"

#: bash_parser.cpp:25 cpp_parser.cpp:31 fortran_parser.cpp:37
#: julia_parser.cpp:29 plugin_katesymbolviewer.cpp:88 python_parser.cpp:25
#: ruby_parser.cpp:21
#, kde-format
msgid "Show Functions"
msgstr "הצג פונקציות"

#: bash_parser.cpp:28 cpp_parser.cpp:43 fortran_parser.cpp:40
#: julia_parser.cpp:55 php_parser.cpp:34 python_parser.cpp:45
#: ruby_parser.cpp:33 tcl_parser.cpp:35
#, kde-format
msgid "Functions"
msgstr "פונקציות"

#: cpp_parser.cpp:29 julia_parser.cpp:27 plugin_katesymbolviewer.cpp:84
#, kde-format
msgid "Show Macros"
msgstr "הצג מקרואים"

#: cpp_parser.cpp:30 julia_parser.cpp:28 plugin_katesymbolviewer.cpp:86
#, kde-format
msgid "Show Structures"
msgstr ""

#: cpp_parser.cpp:41 julia_parser.cpp:56
#, kde-format
msgid "Macros"
msgstr "מאקרואים"

#: cpp_parser.cpp:42 julia_parser.cpp:54
#, kde-format
msgid "Structures"
msgstr ""

#: fortran_parser.cpp:35 perl_parser.cpp:23
#, kde-format
msgid "Show Subroutines"
msgstr "הצג רוטינות משנה"

#: fortran_parser.cpp:36
#, kde-format
msgid "Show Modules"
msgstr "הצג מודולים"

#: fortran_parser.cpp:41 perl_parser.cpp:35
#, kde-format
msgid "Subroutines"
msgstr "הצג רוטינות"

#: fortran_parser.cpp:42
#, kde-format
msgid "Modules"
msgstr "מודולים"

#: perl_parser.cpp:21
#, kde-format
msgid "Show Uses"
msgstr "הצג Uses"

#: perl_parser.cpp:22
#, kde-format
msgid "Show Pragmas"
msgstr "הצג Pragmas"

#: perl_parser.cpp:33
#, kde-format
msgid "Uses"
msgstr "Uses"

#: perl_parser.cpp:34
#, kde-format
msgid "Pragmas"
msgstr "Pragmas"

#: php_parser.cpp:31
#, kde-format
msgid "Namespaces"
msgstr "מתחמים"

#: php_parser.cpp:32
#, kde-format
msgid "Defines"
msgstr "הגדרות"

#: php_parser.cpp:33 python_parser.cpp:44 ruby_parser.cpp:32
#, kde-format
msgid "Classes"
msgstr "מחקלות"

#: plugin_katesymbolviewer.cpp:68
#, kde-format
msgid "SymbolViewer"
msgstr "SymbolViewer"

#: plugin_katesymbolviewer.cpp:77
#, fuzzy, kde-format
#| msgid "List/Tree Mode"
msgid "Tree Mode"
msgstr "מצב עץ או רשימה"

#: plugin_katesymbolviewer.cpp:79
#, kde-format
msgid "Expand Tree"
msgstr ""

#: plugin_katesymbolviewer.cpp:81
#, fuzzy, kde-format
#| msgid "Show Subroutines"
msgid "Show Sorted"
msgstr "הצג רוטינות משנה"

#: plugin_katesymbolviewer.cpp:90
#, fuzzy, kde-format
#| msgid "Show Params"
msgid "Show Parameters"
msgstr "הצג ארגומרנטים"

#: plugin_katesymbolviewer.cpp:116
#, kde-format
msgid "Symbol List"
msgstr "רשימת סמלים"

#: plugin_katesymbolviewer.cpp:134
#, kde-format
msgid "Filter..."
msgstr ""

#: plugin_katesymbolviewer.cpp:144
#, kde-format
msgctxt "@title:column"
msgid "Symbols"
msgstr "סמלים"

#: plugin_katesymbolviewer.cpp:144
#, kde-format
msgctxt "@title:column"
msgid "Position"
msgstr "מיקום"

#: plugin_katesymbolviewer.cpp:362
#, fuzzy, kde-format
#| msgid "Sorry. Language not supported yet"
msgid "Sorry, not supported yet!"
msgstr "שפת התכנות לא נתמכת עוד."

#: plugin_katesymbolviewer.cpp:366
#, kde-format
msgid "File type: %1"
msgstr ""

#: plugin_katesymbolviewer.cpp:479
#, kde-format
msgid "Display functions parameters"
msgstr "הצג ארגומנטים של פונקציות"

#: plugin_katesymbolviewer.cpp:480
#, kde-format
msgid "Automatically expand nodes in tree mode"
msgstr "הרחב באופן אוטומי את הענפים בעץ"

#: plugin_katesymbolviewer.cpp:481
#, kde-format
msgid "Always display symbols in tree mode"
msgstr "הצג סמלים תמיד במצב עץ"

#: plugin_katesymbolviewer.cpp:482
#, kde-format
msgid "Always sort symbols"
msgstr "תמיד מיין סמלים"

#: plugin_katesymbolviewer.cpp:484
#, kde-format
msgid "Parser Options"
msgstr "אפשרויות מפענח"

#: plugin_katesymbolviewer.cpp:512
#, kde-format
msgid "Symbol Viewer"
msgstr "מציג סמלים"

#: plugin_katesymbolviewer.cpp:517
#, kde-format
msgid "Symbol Viewer Configuration Page"
msgstr "עמוד הגדרות מפענח סמלים"

#: python_parser.cpp:26 ruby_parser.cpp:22
#, kde-format
msgid "Show Methods"
msgstr "הצג מתודות"

#: python_parser.cpp:27 ruby_parser.cpp:23
#, kde-format
msgid "Show Classes"
msgstr "הצג מחלקות"

#: tcl_parser.cpp:36
#, kde-format
msgid "Globals"
msgstr "גלובאלים"

#. i18n: ectx: Menu (view)
#: ui.rc:6
#, kde-format
msgid "&Settings"
msgstr "&הגדרות"

#: xml_parser.cpp:25
#, fuzzy, kde-format
#| msgid "Show Pragmas"
msgid "Show Tags"
msgstr "הצג Pragmas"

#: xslt_parser.cpp:22
#, kde-format
msgid "Show Params"
msgstr "הצג ארגומרנטים"

#: xslt_parser.cpp:23
#, kde-format
msgid "Show Variables"
msgstr "הצג משתנים"

#: xslt_parser.cpp:24
#, kde-format
msgid "Show Templates"
msgstr "הצג תבניות"

#: xslt_parser.cpp:34
#, kde-format
msgid "Params"
msgstr "ארגומרנטים"

#: xslt_parser.cpp:35
#, kde-format
msgid "Variables"
msgstr "משתנים"

#: xslt_parser.cpp:36
#, kde-format
msgid "Templates"
msgstr "תבניות"

#~ msgid "Show Globals"
#~ msgstr "הצג גלובאלים"

#~ msgid "Refresh List"
#~ msgstr "רענן רשימה"

#~ msgid "Enable Sorting"
#~ msgstr "אפשר מיון"
