# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
#
# Kristof Kiszel <ulysses@fsf.hu>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-10 01:36+0000\n"
"PO-Revision-Date: 2022-01-25 10:17+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.07.70\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:259
#, kde-format
msgid "Filter..."
msgstr "Szűrő…"

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:99 lspclientconfigpage.cpp:104
#: lspclientpluginview.cpp:452 lspclientpluginview.cpp:607 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr "LSP kliens"

#: lspclientconfigpage.cpp:214
#, kde-format
msgid "No JSON data to validate."
msgstr "Nincs ellenőrzendő JSON-adat."

#: lspclientconfigpage.cpp:223
#, kde-format
msgid "JSON data is valid."
msgstr "A JSON-adat érvényes."

#: lspclientconfigpage.cpp:225
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "A JSON-adat érvénytelen: nincs JSON-objektum"

#: lspclientconfigpage.cpp:228
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "A JSON-adat érvénytelen: %1"

#: lspclientconfigpage.cpp:276
#, kde-format
msgid "Delete selected entries"
msgstr ""

#: lspclientconfigpage.cpp:281
#, kde-format
msgid "Delete all entries"
msgstr ""

#: lspclientplugin.cpp:224
#, kde-format
msgid "LSP server start requested"
msgstr ""

#: lspclientplugin.cpp:227
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""

#: lspclientplugin.cpp:240
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""

#: lspclientpluginview.cpp:419 lspclientpluginview.cpp:671
#, kde-format
msgid "LSP"
msgstr ""

#: lspclientpluginview.cpp:481
#, kde-format
msgid "Go to Definition"
msgstr "Ugrás a definícióra"

#: lspclientpluginview.cpp:483
#, kde-format
msgid "Go to Declaration"
msgstr "Ugrás a deklarációra"

#: lspclientpluginview.cpp:485
#, kde-format
msgid "Go to Type Definition"
msgstr "Ugrés a típusdefinícióra"

#: lspclientpluginview.cpp:487
#, kde-format
msgid "Find References"
msgstr "Hivatkozások keresése"

#: lspclientpluginview.cpp:490
#, kde-format
msgid "Find Implementations"
msgstr "Implementációk keresése"

#: lspclientpluginview.cpp:492
#, kde-format
msgid "Highlight"
msgstr "Kiemelés"

#: lspclientpluginview.cpp:494
#, kde-format
msgid "Symbol Info"
msgstr "Szimbóluminformáció"

#: lspclientpluginview.cpp:496
#, kde-format
msgid "Search and Go to Symbol"
msgstr "Keresés és ugrás szimbólumra"

#: lspclientpluginview.cpp:501
#, kde-format
msgid "Format"
msgstr "Formázás"

#: lspclientpluginview.cpp:504
#, kde-format
msgid "Rename"
msgstr "Átnevezés"

#: lspclientpluginview.cpp:507
#, fuzzy, kde-format
#| msgid "Expand All"
msgid "Expand Selection"
msgstr "Összes kibontása"

#: lspclientpluginview.cpp:510
#, kde-format
msgid "Shrink Selection"
msgstr ""

#: lspclientpluginview.cpp:513
#, kde-format
msgid "Switch Source Header"
msgstr "Váltás a forrásfejlécre"

#: lspclientpluginview.cpp:516
#, fuzzy, kde-format
#| msgid "Expand All"
msgid "Expand Macro"
msgstr "Összes kibontása"

#: lspclientpluginview.cpp:518
#, kde-format
msgid "Code Action"
msgstr "Kódművelet"

#: lspclientpluginview.cpp:533
#, kde-format
msgid "Show selected completion documentation"
msgstr "A kijelölt kiegészítési dokumentáció megjelenítése"

#: lspclientpluginview.cpp:536
#, kde-format
msgid "Enable signature help with auto completion"
msgstr "Aláírás-súgó bekapcsolása automatikus kiegészítéssel"

#: lspclientpluginview.cpp:539
#, kde-format
msgid "Include declaration in references"
msgstr "Deklaráció felvétele a hivatkozásokba"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:542 lspconfigwidget.ui:96
#, kde-format
msgid "Add parentheses upon function completion"
msgstr "Zárójelek hozzáadása függvénykiegészítéskor"

#: lspclientpluginview.cpp:545
#, kde-format
msgid "Show hover information"
msgstr "Információk megjelenítése rámutatáskor"

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:548 lspconfigwidget.ui:47
#, kde-format
msgid "Format on typing"
msgstr "Formázás gépeléskor"

#: lspclientpluginview.cpp:551
#, kde-format
msgid "Incremental document synchronization"
msgstr "Inkrementális dokumentum-szinkronizáció"

#: lspclientpluginview.cpp:554
#, kde-format
msgid "Highlight goto location"
msgstr "Ugrási hely kiemelése"

#: lspclientpluginview.cpp:563
#, kde-format
msgid "Show Inlay Hints"
msgstr ""

#: lspclientpluginview.cpp:567
#, fuzzy, kde-format
#| msgid "Show diagnostics notifications"
msgid "Show Diagnostics Notifications"
msgstr "Diagnosztikai értesítések megjelenítése"

#: lspclientpluginview.cpp:572
#, fuzzy, kde-format
#| msgid "Show messages"
msgid "Show Messages"
msgstr "Üzenetek megjelenítése"

#: lspclientpluginview.cpp:577
#, fuzzy, kde-format
#| msgid "Server memory usage"
msgid "Server Memory Usage"
msgstr "Kiszolgáló memóriahasználata"

#: lspclientpluginview.cpp:581
#, fuzzy, kde-format
#| msgid "Close all dynamic reference tabs"
msgid "Close All Dynamic Reference Tabs"
msgstr "Összes dinamikus hivatkozási lap bezárása"

#: lspclientpluginview.cpp:583
#, kde-format
msgid "Restart LSP Server"
msgstr "LSP kiszolgáló újraindítása"

#: lspclientpluginview.cpp:585
#, kde-format
msgid "Restart All LSP Servers"
msgstr "Összes LSP kiszolgáló újraindítása"

#: lspclientpluginview.cpp:596
#, kde-format
msgid "Go To"
msgstr ""

#: lspclientpluginview.cpp:623
#, kde-format
msgid "More options"
msgstr "További beállítások"

#: lspclientpluginview.cpp:829 lspclientsymbolview.cpp:295
#, kde-format
msgid "Expand All"
msgstr "Összes kibontása"

#: lspclientpluginview.cpp:830 lspclientsymbolview.cpp:296
#, kde-format
msgid "Collapse All"
msgstr "Összes összecsukása"

#: lspclientpluginview.cpp:1033
#, kde-format
msgid "RangeHighLight"
msgstr "Tartománykiemelés"

#: lspclientpluginview.cpp:1345
#, kde-format
msgid "Line: %1: "
msgstr "Sor: %1: "

#: lspclientpluginview.cpp:1497 lspclientpluginview.cpp:1845
#: lspclientpluginview.cpp:1964
#, kde-format
msgid "No results"
msgstr "Nincs találat"

#: lspclientpluginview.cpp:1556
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr "Definíció: %1"

#: lspclientpluginview.cpp:1562
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr "Deklaráció: %1"

#: lspclientpluginview.cpp:1568
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr "Típusdefiníció: %1"

#: lspclientpluginview.cpp:1574
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr "Hivatkozások: %1"

#: lspclientpluginview.cpp:1586
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr "Implementáció: %1"

#: lspclientpluginview.cpp:1599
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr "Kiemelés: %1"

#: lspclientpluginview.cpp:1623 lspclientpluginview.cpp:1634
#: lspclientpluginview.cpp:1647
#, kde-format
msgid "No Actions"
msgstr "Nincsenek műveletek"

#: lspclientpluginview.cpp:1638
#, kde-format
msgid "Loading..."
msgstr "Betöltés…"

#: lspclientpluginview.cpp:1730
#, kde-format
msgid "No edits"
msgstr "Nincsenek szerkesztések"

#: lspclientpluginview.cpp:1804
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "Átnevezés"

#: lspclientpluginview.cpp:1805
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr "Új név (figyelem: nem minden hivatkozás cserélhető)"

#: lspclientpluginview.cpp:1851
#, fuzzy, kde-format
#| msgid "No results"
msgid "Not enough results"
msgstr "Nincs találat"

#: lspclientpluginview.cpp:1920
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr "A megfelelő fejléc/forrás nem található"

#: lspclientpluginview.cpp:2116 lspclientpluginview.cpp:2159
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr "LSP kiszolgáló"

#: lspclientpluginview.cpp:2182
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr "LSP kliens"

#: lspclientpluginview.cpp:2454
#, fuzzy, kde-format
#| msgid "Restart LSP Server"
msgid "Question from LSP server"
msgstr "LSP kiszolgáló újraindítása"

#: lspclientservermanager.cpp:604
#, kde-format
msgid "Restarting"
msgstr "Újraindítás"

#: lspclientservermanager.cpp:604
#, kde-format
msgid "NOT Restarting"
msgstr "NEM indul újra"

#: lspclientservermanager.cpp:605
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr "A kiszolgáló váratlanul leállt … %1 [%2] [honlap: %3] "

#: lspclientservermanager.cpp:801
#, kde-format
msgid "Failed to find server binary: %1"
msgstr "A kiszolgáló binárisa nem található: %1"

#: lspclientservermanager.cpp:804 lspclientservermanager.cpp:846
#, kde-format
msgid "Please check your PATH for the binary"
msgstr "Ellenőrizze a PATH változót a binárishoz"

#: lspclientservermanager.cpp:805 lspclientservermanager.cpp:847
#, kde-format
msgid "See also %1 for installation or details"
msgstr "Lásd %1 a telepítéshez vagy részletekhez"

#: lspclientservermanager.cpp:843
#, kde-format
msgid "Failed to start server: %1"
msgstr "Nem sikerült elindítani a kiszolgálót: %1"

#: lspclientservermanager.cpp:851
#, kde-format
msgid "Started server %2: %1"
msgstr "%2 kiszolgáló elindítva: %1"

#: lspclientservermanager.cpp:886
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""
"Nem sikerült feldolgozni a(z) „%1” kiszolgálóbeállítást: nincs JSON-objektum"

#: lspclientservermanager.cpp:889
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr "Nem sikerült feldolgozni a(z) „%1” kiszolgálóbeállítást: %2"

#: lspclientservermanager.cpp:893
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr "Nem sikerült olvasni a kiszolgálóbeállítást: %1"

#: lspclientsymbolview.cpp:241
#, fuzzy, kde-format
#| msgid "Symbol Outline Options"
msgid "Symbol Outline"
msgstr "Tagolási szimbólum beállításai"

#: lspclientsymbolview.cpp:286
#, kde-format
msgid "Tree Mode"
msgstr "Famód"

#: lspclientsymbolview.cpp:288
#, kde-format
msgid "Automatically Expand Tree"
msgstr "Fa automatikus kibontása"

#: lspclientsymbolview.cpp:290
#, kde-format
msgid "Sort Alphabetically"
msgstr "Rendezés betűrendben"

#: lspclientsymbolview.cpp:292
#, kde-format
msgid "Show Details"
msgstr "Részletek megjelenítése"

#: lspclientsymbolview.cpp:453
#, kde-format
msgid "Symbols"
msgstr "Szimbólumok"

#: lspclientsymbolview.cpp:584
#, kde-format
msgid "No LSP server for this document."
msgstr "Nincs LSP kiszolgáló ehhez a dokumentumhoz."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr "Kliensbeállítások"

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:54
#, fuzzy, kde-format
#| msgid "Format on typing"
msgid "Format on save"
msgstr "Formázás gépeléskor"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Enable semantic highlighting"
msgstr "Szemantikai kiemelés bekapcsolása"

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:68
#, kde-format
msgid "Enable inlay hints"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Completions:"
msgstr "Kiegészítések:"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show inline docs for selected completion"
msgstr "Beágyazott dokumentáció megjelenítése a kijelölt kiegészítéshez"

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:89
#, kde-format
msgid "Show function signature when typing a function call"
msgstr "Függvényaláírás megjelenítése függvényhívás gépelésekor"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:103
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:110
#, kde-format
msgid "Navigation:"
msgstr "Navigáció:"

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:117
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr "Deklarációk számolása szimbólum referenciáinak keresésekor"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:124
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr "Információk megjelenítése a kurzor alatti szimbólumról"

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:131
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr "Célsor kiemelése ugráskor"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:138
#, kde-format
msgid "Server:"
msgstr "Kiszolgáló:"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:145
#, kde-format
msgid "Show program diagnostics"
msgstr "Programdiagnosztikák megjelenítése"

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:152
#, kde-format
msgid "Show notifications from the LSP server"
msgstr "LSP-kiszolgáló értesítéseinek megjelenítése"

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:159
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr "A dokumentumok inkrementális szinkronizálása az LSP-kiszolgálóval-"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:166
#, kde-format
msgid "Document outline:"
msgstr "Dokumentumkinézet:"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:173
#, kde-format
msgid "Sort symbols alphabetically"
msgstr "Szimbólumok rendezése betűrendben"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:180
#, kde-format
msgid "Display additional details for symbols"
msgstr "További részletek megjelenítése a szimbólumhoz"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:187
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr ""
"A szimbólumok hierarchikus formában történő megjelenítése sima lista helyett"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:202
#, kde-format
msgid "Automatically expand tree"
msgstr "Fa automatikus kibontása"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:237
#, kde-format
msgid "User Server Settings"
msgstr "Felhasználói kiszolgálóbeállítások"

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:245
#, kde-format
msgid "Settings File:"
msgstr "Beállításfájl:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:272
#, kde-format
msgid "Default Server Settings"
msgstr "Alapértelmezett kiszolgálóbeállítások"

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr "További beállítások"

#~ msgid "Quick Fix"
#~ msgstr "Gyors javítás"

#, fuzzy
#~| msgid "Show diagnostics highlights"
#~ msgid "Show Diagnostics Highlights"
#~ msgstr "Diagnosztikai kiemelések megjelenítése"

#, fuzzy
#~| msgid "Show diagnostics marks"
#~ msgid "Show Diagnostics Marks"
#~ msgstr "Diagnosztikai jelölések megjelenítése"

#, fuzzy
#~| msgid "Show diagnostics on hover"
#~ msgid "Show Diagnostics on Hover"
#~ msgstr "Diagnosztika megjelenítése rámutatáskor"

#, fuzzy
#~| msgid "Switch to diagnostics tab"
#~ msgid "Switch to Diagnostics Tab"
#~ msgstr "Váltás a diagnosztikai lapra"

#~ msgctxt "@title:tab"
#~ msgid "Diagnostics"
#~ msgstr "Diagnosztika"

#~ msgid "Error"
#~ msgstr "Hiba"

#~ msgid "Warning"
#~ msgstr "Figyelmeztetés"

#~ msgid "Information"
#~ msgstr "Információ"

#~ msgctxt "@info"
#~ msgid ""
#~ "Error in regular expression: %1\n"
#~ "offset %2: %3"
#~ msgstr ""
#~ "Hiba a reguláris kifejezésben: %1\n"
#~ "%2 eltolás: %3"

#~ msgid "Copy to Clipboard"
#~ msgstr "Másolás a vágólapra"

#~ msgid "Remove Global Suppression"
#~ msgstr "Globális rejtés törlése"

#~ msgid "Add Global Suppression"
#~ msgstr "Globális rejtés hozzáadása"

#~ msgid "Remove Local Suppression"
#~ msgstr "Helyi rejtés törlése"

#~ msgid "Add Local Suppression"
#~ msgstr "Helyi rejtés hozzáadása"

#~ msgid "Disable Suppression"
#~ msgstr "Rejtés kikapcsolása"

#~ msgid "Enable Suppression"
#~ msgstr "Rejtés bekapcsolása"

#~ msgctxt "@info"
#~ msgid "%1 [suppressed: %2]"
#~ msgstr "%1 [rejtve: %2]"

#~ msgid "Diagnostics:"
#~ msgstr "Diagnosztika:"

#~ msgid "Highlight lines with diagnostics"
#~ msgstr "Diagnosztikai sorok kiemelése"

#~ msgid "Show markers in the margins for lines with diagnostics"
#~ msgstr "Jelölők megjelenítése a szegélyen diagnosztikai sorokhoz"

#~ msgid "Show diagnostics on hover"
#~ msgstr "Diagnosztika megjelenítése rámutatáskor"

#~ msgid "max diagnostics tooltip size"
#~ msgstr "diagnosztikai súgó maximális mérete"

#~ msgid "Type to filter through symbols in your project..."
#~ msgstr "Írjon ide a projekt szimbólumainak szűréséhez…"

#~ msgid "LSP Client Symbol Outline"
#~ msgstr "LSP kliens tagolási szimbólum"

#~ msgid "General Options"
#~ msgstr "Általános beállítások"

#~ msgid "Add highlights"
#~ msgstr "Kiemelések hozzáadása"

#~ msgid "Add markers"
#~ msgstr "Jelölők hozzáadása"

#~ msgid "On hover"
#~ msgstr "Rámutatáskor"

#~ msgid "Tree mode outline"
#~ msgstr "Famód tagolás"

#~ msgid "Automatically expand nodes in tree mode"
#~ msgstr "Csomópontok automatikus kibontása famódban"
