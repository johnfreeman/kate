# Translation of katefiletree to Norwegian Bokmål
#
# Knut Yrvin <knut.yrvin@gmail.com>, 2004.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2004, 2009, 2010, 2011, 2012, 2013, 2014.
# Sven Harald Klein Bakke <sirius@nonline.org>, 2004.
# Axel Bojer <fri_programvare@bojer.no>, 2004, 2005, 2006, 2007.
# Nils Kristian Tomren <slx@nilsk.net>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-13 01:39+0000\n"
"PO-Revision-Date: 2014-11-11 22:20+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: katefiletree.cpp:120
#, kde-format
msgctxt "@action:inmenu"
msgid "Reloa&d"
msgstr "&Last inn på nytt"

#: katefiletree.cpp:122
#, kde-format
msgid "Reload selected document(s) from disk."
msgstr "Last valgte dokument(er) fra disk på nytt."

#: katefiletree.cpp:124
#, kde-format
msgctxt "@action:inmenu"
msgid "Close"
msgstr "Lukk"

#: katefiletree.cpp:126
#, kde-format
msgid "Close the current document."
msgstr "Lukk det gjeldende dokumentet."

#: katefiletree.cpp:128
#, kde-format
msgctxt "@action:inmenu"
msgid "Expand Recursively"
msgstr ""

#: katefiletree.cpp:130
#, kde-format
msgid "Expand the file list sub tree recursively."
msgstr ""

#: katefiletree.cpp:132
#, kde-format
msgctxt "@action:inmenu"
msgid "Collapse Recursively"
msgstr ""

#: katefiletree.cpp:134
#, kde-format
msgid "Collapse the file list sub tree recursively."
msgstr ""

#: katefiletree.cpp:136
#, kde-format
msgctxt "@action:inmenu"
msgid "Close Other"
msgstr "Lukk andre"

#: katefiletree.cpp:138
#, kde-format
msgid "Close other documents in this folder."
msgstr "Lukk andre dokumenter i denne mappa."

#: katefiletree.cpp:141
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Containing Folder"
msgstr ""

#: katefiletree.cpp:143
#, kde-format
msgid "Open the folder this file is located in."
msgstr ""

#: katefiletree.cpp:145
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Location"
msgstr ""

#: katefiletree.cpp:147
#, kde-format
msgid "Copy path and filename to the clipboard."
msgstr ""

#: katefiletree.cpp:149
#, kde-format
msgctxt "@action:inmenu"
msgid "Rename..."
msgstr ""

#: katefiletree.cpp:151
#, kde-format
msgid "Rename the selected file."
msgstr "Fjern den valgte fila."

#: katefiletree.cpp:154
#, kde-format
msgid "Print selected document."
msgstr "Skriv ut det valgte dokumentet."

#: katefiletree.cpp:157
#, kde-format
msgid "Show print preview of current document"
msgstr "Vis forhåndsvisning av gjeldende dokument"

#: katefiletree.cpp:159
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr ""

#: katefiletree.cpp:161
#, kde-format
msgid "Close and delete selected file from storage."
msgstr "Lukk og slett  valgte fil fra lageret."

#: katefiletree.cpp:165
#, kde-format
msgctxt "@action:inmenu"
msgid "Clear History"
msgstr "Tøm historien"

#: katefiletree.cpp:167
#, kde-format
msgid "Clear edit/view history."
msgstr "Tøm rediger/vis-historien"

#: katefiletree.cpp:230
#, kde-format
msgctxt "@action:inmenu"
msgid "Tree Mode"
msgstr "Tre-visning"

#: katefiletree.cpp:231
#, kde-format
msgid "Set view style to Tree Mode"
msgstr "Sett visningsstil til tre-visning"

#: katefiletree.cpp:237
#, kde-format
msgctxt "@action:inmenu"
msgid "List Mode"
msgstr "Liste-visning"

#: katefiletree.cpp:238
#, kde-format
msgid "Set view style to List Mode"
msgstr "Sett visningsstil til liste-visning"

#: katefiletree.cpp:245
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Name"
msgstr "Dokumentnavn"

#: katefiletree.cpp:246
#, kde-format
msgid "Sort by Document Name"
msgstr "Sorter etter dokumentnavn"

#: katefiletree.cpp:251
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Path"
msgstr "Dokumentsti"

#: katefiletree.cpp:251
#, kde-format
msgid "Sort by Document Path"
msgstr "Sorter etter dokumentsti"

#: katefiletree.cpp:255
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Opening Order"
msgstr "Åpningsrekkefølge"

#: katefiletree.cpp:256
#, kde-format
msgid "Sort by Opening Order"
msgstr "Sorter etter åpningsrekkefølge"

#: katefiletree.cpp:259 katefiletreeconfigpage.cpp:73
#, kde-format
msgid "Custom Sorting"
msgstr ""

#: katefiletree.cpp:388
#, kde-format
msgctxt "@action:inmenu"
msgid "Open With"
msgstr "Åpne med"

#: katefiletree.cpp:402
#, kde-format
msgid "Show File Git History"
msgstr ""

#: katefiletree.cpp:441
#, kde-format
msgctxt "@action:inmenu"
msgid "View Mode"
msgstr "Visningsmodus"

#: katefiletree.cpp:445
#, kde-format
msgctxt "@action:inmenu"
msgid "Sort By"
msgstr "Sorter etter"

#: katefiletreeconfigpage.cpp:44
#, kde-format
msgid "Background Shading"
msgstr "Bakgrunnsskygge"

#: katefiletreeconfigpage.cpp:51
#, kde-format
msgid "&Viewed documents' shade:"
msgstr "Skygge for &viste dokumenter:"

#: katefiletreeconfigpage.cpp:57
#, kde-format
msgid "&Modified documents' shade:"
msgstr "Skygge for &endrede dokumenter:"

#: katefiletreeconfigpage.cpp:65
#, kde-format
msgid "&Sort by:"
msgstr "Sorter &etter:"

#: katefiletreeconfigpage.cpp:70
#, kde-format
msgid "Opening Order"
msgstr "Åpningsrekkefølge"

#: katefiletreeconfigpage.cpp:71
#, kde-format
msgid "Document Name"
msgstr "Dokumentnavn"

#: katefiletreeconfigpage.cpp:72
#, kde-format
msgid "Url"
msgstr "Nettadresse"

#: katefiletreeconfigpage.cpp:78
#, kde-format
msgid "&View Mode:"
msgstr "&Visningsmodus:"

#: katefiletreeconfigpage.cpp:83
#, kde-format
msgid "Tree View"
msgstr "Trevisning"

#: katefiletreeconfigpage.cpp:84
#, kde-format
msgid "List View"
msgstr "Listevisning"

#: katefiletreeconfigpage.cpp:89
#, kde-format
msgid "&Show Full Path"
msgstr "Vis full &sti"

#: katefiletreeconfigpage.cpp:94
#, kde-format
msgid "Show &Toolbar"
msgstr ""

#: katefiletreeconfigpage.cpp:97
#, kde-format
msgid "Show Close Button"
msgstr ""

#: katefiletreeconfigpage.cpp:99
#, kde-format
msgid ""
"When enabled, this will show a close button for opened documents on hover."
msgstr ""

#: katefiletreeconfigpage.cpp:104
#, kde-format
msgid ""
"When background shading is enabled, documents that have been viewed or "
"edited within the current session will have a shaded background. The most "
"recent documents have the strongest shade."
msgstr ""
"Når bakgrunnsskygge er aktivt, får dokumenter som er vist eller redigert i "
"den åpne økta en skyggelagt bakgrunn. De nyeste dokumentene har sterkest "
"skygge."

#: katefiletreeconfigpage.cpp:107
#, kde-format
msgid "Set the color for shading viewed documents."
msgstr "Sett farge for skyggelegging av viste dokumenter."

#: katefiletreeconfigpage.cpp:109
#, kde-format
msgid ""
"Set the color for modified documents. This color is blended into the color "
"for viewed files. The most recently edited documents get most of this color."
msgstr ""
"Oppgi farge for dokumenter som er blitt endret. Denne fargen går over i "
"fargen for viste filer. De filene som sist ble endret får mest av denne "
"fargen."

#: katefiletreeconfigpage.cpp:114
#, kde-format
msgid ""
"When enabled, in tree mode, top level folders will show up with their full "
"path rather than just the last folder name."
msgstr ""
"Når dette er slått på, blir mapper på toppnivå i trevisning vist med fullt "
"stinavn i stedet for bare det siste mappenavnet."

#: katefiletreeconfigpage.cpp:117
#, kde-format
msgid ""
"When enabled, a toolbar with actions like “Save” are displayed above the "
"list of documents."
msgstr ""

#: katefiletreeconfigpage.cpp:136 katefiletreeplugin.cpp:127
#: katefiletreeplugin.cpp:134
#, kde-format
msgid "Documents"
msgstr "Dokumenter"

#: katefiletreeconfigpage.cpp:141
#, kde-format
msgid "Configure Documents"
msgstr "Sett opp dokumenter"

#: katefiletreemodel.cpp:494
#, kde-format
msgctxt ""
"Open here is a description, i.e. 'list of widgets that are open' not a verb"
msgid "Open Widgets"
msgstr ""

#: katefiletreemodel.cpp:644
#, kde-format
msgctxt "%1 is the full path"
msgid ""
"<p><b>%1</b></p><p>The document has been modified by another application.</p>"
msgstr "<p><b>%1</b></p><p>Dokumentet er blitt endret av et annet program.</p>"

#: katefiletreeplugin.cpp:248
#, kde-format
msgid "Previous Document"
msgstr "Forrige dokument"

#: katefiletreeplugin.cpp:254
#, kde-format
msgid "Next Document"
msgstr "Neste dokument"

#: katefiletreeplugin.cpp:260
#, kde-format
msgid "&Show Active Document"
msgstr ""

#: katefiletreeplugin.cpp:265
#, kde-format
msgid "Save"
msgstr ""

#: katefiletreeplugin.cpp:266
#, kde-format
msgid "Save the current document"
msgstr "Lagre det gjeldende dokumentet"

#: katefiletreeplugin.cpp:270
#, kde-format
msgid "Save As"
msgstr ""

#: katefiletreeplugin.cpp:271
#, kde-format
msgid "Save the current document under a new name"
msgstr ""

#. i18n: ectx: Menu (go)
#: ui.rc:7
#, kde-format
msgid "&Go"
msgstr ""
